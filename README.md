# notes-app

Applicacion web de ABM de notas, hecho con React(Typescript) y Spring Boot (Java).

## Descripcion

La aplicación web es un simple ABM de notas. Permite:

- Crear notas
- Editar notas
- Borrar notas
- Obtener una o todas las notas

Ademas, mantiene la última fecha de edicion de cada nota.

## Instalación

Para el __backend__, importar la carpeta backend como un Maven project, y hacer un update o un Maven build para descargar las dependencias. Ademas, en el archivo ```src/main/resources/application.properties```, ingresar las credenciales de una BD válida.

## Uso


## Diario

- Día 1: Capacitación en tecnologias.
- Día 2: Creación de la api REST en Spring Boot.

