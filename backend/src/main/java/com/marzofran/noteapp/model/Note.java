package com.marzofran.noteapp.model;

import java.time.LocalDateTime;

import javax.persistence.*;

import lombok.*;


@NoArgsConstructor
@Entity
@Table(name="NOTAS")
@Data public class Note implements java.io.Serializable {

	private static final long serialVersionUID = 5808651729005721049L;
	
	@Id @GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="NOTA_ID")
	private Long id;
	
	@Column(name="TITULO")
	private String title;
	
	@Column(name="CONTENIDO", nullable=false)
	@NonNull private String content;
	
	@Column(name="FECHA_CREACION", nullable=false)
	private LocalDateTime creation = LocalDateTime.now();
	
	@Column(name="FECHA_ULTIMA_EDICION")
	private LocalDateTime lastEdit = null;
	
	public Note(Long id, String title, String content) {
		this.id = id;
		this.title = title;
		this.content = content;
	}
}
