package com.marzofran.noteapp.dto;

import org.springframework.stereotype.Component;

import com.marzofran.noteapp.model.Note;

@Component
public class NoteMapper {
	
	public NoteDTO toDto(Note note) {
		return new NoteDTO(note.getTitle(), note.getContent());
	}
	
	public Note toNote(NoteDTO noteDTO) {
		return new Note(null, noteDTO.getTitle(), noteDTO.getContent());
	}

}
