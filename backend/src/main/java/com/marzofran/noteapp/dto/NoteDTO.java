package com.marzofran.noteapp.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NonNull;

@Data
@AllArgsConstructor
public class NoteDTO {

	private String title;
	@NonNull private String content;
	
}
