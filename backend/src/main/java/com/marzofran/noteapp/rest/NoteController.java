package com.marzofran.noteapp.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.marzofran.noteapp.dto.NoteDTO;
import com.marzofran.noteapp.dto.NoteMapper;
import com.marzofran.noteapp.model.Note;
import com.marzofran.noteapp.service.NoteService;

@RestController
@RequestMapping(value="/notas")
public class NoteController {

	@Autowired
	NoteService noteService;
	
	@Autowired
	NoteMapper noteMapper;
	
	@GetMapping
	public ResponseEntity<?> getNotes() {
		List<Note> cache = noteService.getAll();
		
		if (cache.isEmpty()) return new ResponseEntity<String>("No hay ninguna nota.", HttpStatus.NO_CONTENT);
		return new ResponseEntity<List<Note>>(cache, HttpStatus.OK);
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<?> getOneNote(@PathVariable("id") Long id) {
		if (id == null) return new ResponseEntity<String>("Id invalido", HttpStatus.BAD_REQUEST);
		Note note = noteService.findById(id);
		
		if (note == null) return new ResponseEntity<String>("Nota no encontrada", HttpStatus.NO_CONTENT);

		return new ResponseEntity<Note>(note, HttpStatus.OK);
	}
	
	@PostMapping
	public ResponseEntity<Note> createNote(@RequestBody NoteDTO noteDTO ) {
		Note note = noteService.create(noteMapper.toNote(noteDTO));
		return new ResponseEntity<Note>(note, HttpStatus.CREATED);
	}
	
	@PutMapping("/{id}")
	public ResponseEntity<?> updateNote(@PathVariable("id") Long id, @RequestBody NoteDTO noteDTO) {
		if (id == null) return new ResponseEntity<String>("Id invalido", HttpStatus.BAD_REQUEST);
		Note note = noteService.findById(id);
		if ( note == null) return new ResponseEntity<String>("Nota no encontrada", HttpStatus.NO_CONTENT);
		
		note.setTitle(noteDTO.getTitle());
		note.setContent(noteDTO.getContent());
		
		noteService.update(note);
		return new ResponseEntity<Note>(note, HttpStatus.OK);
	}
	
	@DeleteMapping("/{id}")
	public ResponseEntity<String> deleteNote(@PathVariable("id") Long id) {
		if (id == null) return new ResponseEntity<String>("Id invalido", HttpStatus.BAD_REQUEST);
		if (noteService.findById(id) == null) return new ResponseEntity<String>("Nota no encontrada", HttpStatus.NO_CONTENT);
		
		noteService.deleteById(id);
		return new ResponseEntity<String>("Nota borrada correctamente", HttpStatus.OK);
	}
	
}
