package com.marzofran.noteapp.service;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.stereotype.Service;

import com.marzofran.noteapp.model.*;

@Service
@Transactional
public class NoteService {

	@Autowired
	NoteRepository noteRepository;
	
	public Note findById(Long id) {
		return noteRepository.findById(id).orElse(null);
	}
	
	public List<Note> getAll(){
		return noteRepository.findAll();
	}
	
	public Note create(Note note) {
		return noteRepository.save(note);
	}
	
	public void update(Note note) {
		note.setLastEdit(LocalDateTime.now());
		noteRepository.save(note);
	}
	
	public void deleteById(Long id) {
		noteRepository.deleteById(id);
	}
	
}
